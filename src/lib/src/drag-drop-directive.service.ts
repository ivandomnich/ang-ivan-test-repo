import { Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class DragDropDirectiveService {
	private dropItem = new Subject<any>();
  private draggedItem:any;
  constructor() { }
  public setDropItem(item){
  	this.dropItem.next(item);
  }
 	public getDropItem(){
  	return this.dropItem.asObservable();
  }
  public setDragItem(item){
    this.draggedItem = item;
  }
  public getDragItem(){
    return this.draggedItem;
  }
}
