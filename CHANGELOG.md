# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.0.4"></a>
## [1.0.4](https://bitbucket.org/ivandomnich/ang-ivan-test-repo/compare/v1.0.3...v1.0.4) (2018-04-10)



<a name="1.0.3"></a>
## [1.0.3](https://bitbucket.org/ivandomnich/ang-ivan-test-repo/compare/v1.0.2...v1.0.3) (2018-04-10)



<a name="1.0.2"></a>
## [1.0.2](https://bitbucket.org/ivandomnich/ang-ivan-test-repo/compare/v1.0.1...v1.0.2) (2018-04-10)



<a name="1.0.1"></a>
## 1.0.1 (2018-04-10)
